require 'json' 

class Hash 
    def to_css 
        lines = ""
        self.each do |rule, style| 
            lines << "#{rule} {"
            style.each do |key, value| 
                lines << "#{key}: #{value.inspect}"
            end 
            lines << "}"
        end 
    end 
end 